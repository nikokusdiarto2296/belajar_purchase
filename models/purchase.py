# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    supplier_phone = fields.Char(related='partner_id.phone')
    supplier_email = fields.Char(related='partner_id.email')
    vendor_ids = fields.Many2many('res.partner')
    duration = fields.Float(compute='_compute_duration')
    is_reference = fields.Boolean()

    """Kumpulan Fungsi"""
    @api.depends('date_order', 'date_planned')
    def _compute_duration(self):
        for rec in self:
            if rec.date_order and rec.date_planned:
                date_planned_temp = datetime(int(rec.date_planned.year), int(rec.date_planned.month), int(rec.date_planned.day))
                date_order_temp = datetime(int(rec.date_order.year), int(rec.date_order.month), int(rec.date_order.day))
                interval = date_planned_temp - date_order_temp
                rec.duration = interval.days
            else:
                rec.duration = 0